export default {
    user: {},
    summary: null,
    articles: {
        data: [],
        selectedArticle: null
    },
    photos: {
        data: []
    },
    videos: {
        data: []
    }
}