import {AppLogin, AppResetPassword, AppForgotPassword, AppMasterLayout, AppDashboard, AppArticles, AppMedia, AppArticlePage, AppEditArticle, AppAddArticle, AppMediaPictures, AppMediaVideos, AddPicture, AddVideo, EditPicture, EditVideo} from './../components'

export default [
    {
      path: '/',
      name: 'login',
      meta: {title: 'Login to your account'},
      component: AppLogin
    },
    {
      path: '/password/reset',
      name: 'reset-password',
      meta: {title: 'Reset Your Password'},
      component: AppResetPassword
    },
    {
      path: '/password/forgot',
      name: 'forgot-password',
      meta: {title: 'Forgot Your Password?'},
      component: AppForgotPassword
    },
    {
      path: '/admin',
      name: 'admin',
      meta: {title: 'Welcome back', auth: true},
      component: AppMasterLayout,
      children: [
        {
          name: 'dashboard',
          path: 'dashboard',
          meta: {title: 'Dashboard'},
          component: AppDashboard
        },
        {
          name: 'articles',
          path: 'articles',
          meta: {title: 'Articles'},
          component: AppArticles
        },
        {
          name: 'media',
          path: 'media',
          meta: {title: 'Media'},
          component: AppMedia
        },
        {
          name: 'article-page',
          path: 'article',
          meta: {title: 'View Article', link: 'article'},
          component: AppArticlePage
        },
        {
          name: 'edit-article',
          path: 'edit-article',
          meta: {title: 'Edit Article', link: 'article'},
          component: AppEditArticle
        },
        {
          name: 'add-article',
          path: 'add-article',
          component: AppAddArticle,
          meta: {title: 'Add Article', link: 'article'}
        },
        {
          name: 'pictures',
          path: 'media/pictures',
          meta: {title: 'Media > Pictures', link: 'media'},
          component: AppMediaPictures
        },
        {
          name: 'videos',
          path: 'media/videos',
          meta: {title: 'Media > Videos', link: 'media'},
          component: AppMediaVideos
        },
        {
          name: 'add-picture',
          path: '/media/pictures/add',
          meta: {title: 'Media > Pictures > New', parent: 'picture'},
          component: AddPicture
        },
        {
          name: 'edit-picture',
          path: '/media/pictures/edit',
          meta: {title: 'Media > Pictures > Edit', parent: 'picture'},
          component: EditPicture
        },
        {
          name: 'add-video',
          path: '/media/videos/add',
          meta: {title: 'Media > Videos > New', parent: 'video'},
          component: AddVideo
        },
        {
          name: 'edit-video',
          path: '/media/videos/edit',
          meta: {title: 'Media > Videos > Edit', parent: 'video'},
          component: EditVideo
        }
      ]
    }
]