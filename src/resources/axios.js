import Axios from 'axios'
import Vue from 'vue'

const axios = Axios.create({
    baseURL: 'http://fanpag-api.local/api/'
})

axios.interceptors.request.use(function (config) {

    let apiToken = Vue.auth.getToken()
    if (apiToken && !config.headers.common.Authorization) {
        config.headers.common.Authorization = `Bearer ${apiToken}`
    }

    NProgress.start()
    return config;
    
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    NProgress.done()
    return response
}, function (error) {
    NProgress.done()
    return error
})

export default axios