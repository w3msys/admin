import VeeValidate from 'vee-validate'
import Auth from './auth'
import GlobalComponents from './global-components'
import axios from './../resources/axios'
import VueAxios from 'vue-axios'

export default {
    install (Vue) {
        Vue.use(VeeValidate)
        Vue.use(Auth)
        Vue.use(GlobalComponents)
        Vue.use(VueAxios, axios)
    }
}