export default {
    setUser ({commit}, user) {
        commit('SET_USER', user)
    },

    removeArticle ({commit}, id) {
        commit('REMOVE_ARTICLE', id)
    },

    setSelectedArticle ({commit}, payload) {
        commit('SET_SELECTED_ARTICLE', payload)
    },
    
    setSummary ({commit}, summary) {
        commit('SET_SUMMARY', summary)
    },

    setArticles ({commit}, articles) {
        commit('SET_ARTICLES', articles)
    },

    addArticle ({commit}, article) {
        commit('ADD_ARTICLE', article)
    },

    setPhotos ({commit}, photos) {
        commit('SET_PHOTOS', photos)
    },

    addPhoto ({commit}, photo) {
        commit('ADD_PHOTO', photo)
    },

    removePhoto ({commit}, id) {
        commit('REMOVE_PHOTO', id)
    },

    setVideos ({commit}, videos) {
        commit('SET_VIDEOS', videos)
    },

    removeVideo ({commit}, id) {
        commit('REMOVE_VIDEO', id)
    }
}