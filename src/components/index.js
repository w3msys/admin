import login from './authentication/login.vue'
import resetPassword from './authentication/reset-password.vue'
import forgotPassword from './authentication/forgot-password.vue'

import master from './layout/master.vue'

import dashboard from './dashboard/dashboard.vue'
import articles from './articles/articles.vue'
import media from './media/media.vue'
import singleArticle from './articles/article-page.vue'
import editArticle from './articles/edit-article.vue'
import addArticle from './articles/add-article.vue'
import pictures from './media/pictures.vue'
import videos from './media/videos.vue'
import addPicture from './media/add-picture.vue'
import addVideo from './media/add-video.vue'
import editPicture from './media/edit-picture.vue'
import editVideo from './media/edit-video.vue'

export const AppLogin = login
export const AppResetPassword = resetPassword
export const AppForgotPassword = forgotPassword


export const AppMasterLayout = master

export const AppDashboard = dashboard
export const AppArticles = articles
export const AppMedia = media
export const AppArticlePage = singleArticle
export const AppEditArticle = editArticle
export const AppAddArticle = addArticle
export const AppMediaPictures = pictures
export const AppMediaVideos = videos
export const AddPicture = addPicture
export const AddVideo = addVideo
export const EditPicture = editPicture
export const EditVideo = editVideo