export default {
    install (Vue) {

        let name = 'fp_osj3f'

        Vue.auth = {
            setToken (token) {
                localStorage.setItem(name, token)
            },

            getToken () {
                let token = localStorage.getItem(name)
                if (token) return token
                return null
            },

            destroyToken () {
                localStorage.removeItem(name)
                return true
            },

            isAuthenticated () {
               return !!this.getToken()
            }
        }

        Vue.prototype.$auth = Vue.auth
    }
}