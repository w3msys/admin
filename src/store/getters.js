export default {
    getUser (state) {
        return state.user
    },

    getSummary (state) {
        return state.summary
    },

    getArticles (state) {
        return state.articles
    },

    getSelectedArticle (state) {
        return state.articles.selectedArticle
    },
    
    getPhotos (state) {
        return state.photos
    },

    getVideos (state) {
        return state.videos
    }
}