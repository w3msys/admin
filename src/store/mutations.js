export default {
    SET_USER (state, user) {
        state.user = user
    },
    
    REMOVE_ARTICLE (state, id) {
        for(let i = 0; i < state.articles.data.length; i++) {
            if (state.articles.data[i].id == id) {
                state.articles.data.splice(i, 1)
            }
        }
    },

    SET_SELECTED_ARTICLE (state, payload) {
        if (payload.by == 'id') {
            state.articles.selectedArticle = state.articles.data.find(item => {
                return item.id == payload.value
            })
        } else {
            state.articles.selectedArticle = payload.value
        }
    },

    SET_SUMMARY (state, summary) {
        state.summary = summary
    },

    SET_ARTICLES (state, articles){
        state.articles = {...state.articles, ...articles}
    },

    ADD_ARTICLE (state, article){
        state.articles.data.unshift(article)
    },

    SET_PHOTOS (state, photos) {
        state.photos = {...state.photos, ...photos}
    },

    ADD_PHOTO (state, photo) {
        state.photos.data.unshift(photo)
    },

    REMOVE_PHOTO (state, id) {
        for(let i = 0; i < state.photos.data.length; i++) {
            if (state.photos.data[i].id == id) {
                state.photos.data.splice(i, 1);
                return false
            }
        }
    },

    SET_VIDEOS (state, videos) {
        state.videos = {...state.videos, ...videos}
    },

    REMOVE_VIDEO (state, id) {
        for (let i = 0; i < state.videos.data.length; i++) {
            if (state.videos.data[i].id == id) {
                state.videos.data.splice(i, 1)
                return false
            }
        }
    }
}